# eltexsoft-stylelint-config

[![NPM version](http://img.shields.io/npm/v/eltexsoft-stylelint-config.svg)](https://www.npmjs.org/package/eltexsoft-stylelint-config) [![Build status](https://ci.appveyor.com/api/projects/status/tvd8r3aspwi5m85a?svg=true)](https://ci.appveyor.com/project/eltexsoft/stylelint-config/branch/master)

> The standard shareable config for stylelint.

Extends [`stylelint-config-standard`](https://github.com/stylelint/stylelint-config-standard).

Turns on additional rules to enforce the common stylistic conventions found within a handful of CSS styleguides, including: [The Idiomatic CSS Principles](https://github.com/necolas/idiomatic-css),
[Google's CSS Style Guide](https://google.github.io/styleguide/htmlcssguide.html#CSS_Formatting_Rules), [Airbnb's Styleguide](https://github.com/airbnb/css#css), and [@mdo's Code Guide](http://codeguide.co/#css).

It favours flexibility over strictness for things like multi-line lists and single-line rulesets, and tries to avoid potentially divisive rules.

Use it as is or as a foundation for your own config.

To see the rules that this config uses, please read the [config itself](./index.js).

## Example

```css
@import url(x.css);
@import url(y.css);

/**
 * Multi-line comment
 */

.selector-1,
.selector-2,
.selector-3[type="text"] {
    background: linear-gradient(#fff, rgba(0, 0, 0, 0.8));
    box-sizing: border-box;
    display: block;
    color: #333;
}

.selector-a,
.selector-b:not(:first-child) {
    padding: 10px !important;
    top: calc(calc(1em * 2) / 3);
}

.selector-x {
    width: 10%;
}

.selector-y {
    width: 20%;
}

.selector-z {
    width: 30%;
}

/* Single-line comment */

@media (min-width >= 60em) {
    .selector {
        /* Flush to parent comment */
        transform: translate(1, 1) scale(3);
    }
}

@media (orientation: portrait), projection and (color) {
    .selector-i + .selector-ii {
        background: color(rgb(0, 0, 0) lightness(50%));
        font-family: helvetica, "arial black", sans-serif;
    }
}

/* Flush single line comment */
@media screen and (min-resolution: 192dpi), screen and (min-resolution: 2dppx) {
    .selector {
        background-image:
            repeating-linear-gradient(
                -45deg,
                transparent,
                #fff 25px,
                rgba(255, 255, 255, 1) 50px
            );
        margin: 10px;
        margin-bottom: 5px;
        box-shadow:
            0 1px 1px #000,
            0 1px 0 #fff,
            2px 2px 1px 1px #ccc inset;
        height: 10rem;
    }

    /* Flush nested single line comment */
    .selector::after {
        content: '→';
        background-image: url(x.svg);
    }
}
```

*Note: the config is tested against this example, as such the example contains plenty of CSS syntax, formatting and features.*

## Installation

```bash
npm install eltexsoft-stylelint-config --save-dev
```
or
```bash
yarn add eltexsoft-stylelint-config --dev
```

## Usage

If you've installed `eltexsoft-stylelint-config` locally within your project, just set your `stylelint` config to:

```json
{
    "extends": "eltexsoft-stylelint-config"
}
```

If you've globally installed `eltexsoft-stylelint-config` using the `-g` flag, then you'll need to use the absolute path to `eltexsoft-stylelint-config` in your config e.g.

```json
{
    "extends": "/absolute/path/to/eltexsoft-stylelint-config"
}
```

Since [stylelint 9.7.0](https://github.com/stylelint/stylelint/blob/9.7.0/CHANGELOG.md#970), you can simply use the globally installed configuration name instead of the absolute path:

```json
{
    "extends": "eltexsoft-stylelint-config"
}
```

### Extending the config

Simply add a `"rules"` key to your config, then add your overrides and additions there.

For example, to change the `at-rule-no-unknown` rule to use its `ignoreAtRules` option, change the `indentation` to tabs, turn off the `number-leading-zero` rule,and add the `unit-whitelist` rule:

```json
{
    "extends": "eltexsoft-stylelint-config",
    "rules": {
        "at-rule-no-unknown": [ true, {
            "ignoreAtRules": [
              "extends",
              "ignores"
            ]
        }],
        "number-leading-zero": null,
        "unit-whitelist": ["em", "rem", "s"]
    }
}
```

## [Changelog](CHANGELOG.md)

## [License](LICENSE)
